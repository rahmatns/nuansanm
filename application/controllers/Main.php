<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
	protected $data = array();
	public function __construct() {
	    parent::__construct();
	    $this->load->model('product_model');
	    $this->load->model('company_model');
	    $company_list = $this->company_model->search(array(),1);
	    $company = array('name' => "PT. Nuansa Nisa Met",
	    				 'email' => "nuansanisa@yahoo.com",
	    				 'phone' => "(021) 826 104 28",
	    				 'address' => "Jl. Raya Narogong Pangkalan 2 No. 78 Bantarge",
	    				 'image_url' => 'assets/images/homepage/ic_odoo3.png',
	    				 'display_name' => 'Jasa Bodem dan Roll Plate',
	    				 'description' => 'PT. Nuansa Nisa Met menerima pengerjaan Pembuatan tangki, Bodem, dan Roll Plat');
	    if(count($company_list) > 0){
	    	$company = $company_list[0];
	    }
	    $this->data['company'] = $company;
	}
	public function index()
	{
		$this->load->model('testimony_model');
		$this->load->model('slideshow_model');
		$testimony_where = array('enable' => 1);
		$slides_where = array('enable' => 1);
		$product_where = array('enable' => 1);
		$this->data['testimonies'] = $this->testimony_model->search($testimony_where,FALSE,"enable desc, sequence asc");
		$this->data['slides'] = $this->slideshow_model->search($slides_where);
		$this->data['product_list'] = $this->product_model->search($product_where,FALSE,"enable desc");
		$this->load->view('welcome_message',$this->data);
		$this->load->view('footer');
	}

	public function products()
	{
		$this->data['product_list'] = $this->product_model->search(array());
		$this->load->view('products',$this->data);
		$this->load->view('footer');
	}

	public function product($string_slug_id)
	{
		$id = explode("-", $string_slug_id)[0];
		$this->data['product'] = $this->product_model->browse($id);
		$this->load->view('product_detail',$this->data);
		$this->load->view('footer');
	}

	public function gallery()
	{
	    $this->load->model('gallery_model');
		$this->data['galleries'] = $this->gallery_model->search(array());
		$this->load->view('gallery',$this->data);
		$this->load->view('footer');
	}

	public function about()
	{
		$this->load->model('about_model');
		$records = $this->about_model->search(array(),1);
		if(count($records) == 0){
			$this->load->view('not-found',$this->data);
		}else{
			$this->data['about'] = $records[0];
			$this->load->view('about',$this->data);
		}
		$this->load->view('footer');
	}

	public function post_contact()
	{
		if($this->input->post()){
			$data = array('name' => $this->input->post('name'),
			 			  'email' => $this->input->post('email'),
			 			  'message' => $this->input->post('message')
			 			);
			$this->load->model('contact_model');
			$insert = $this->contact_model->insert($data);
			if ($insert){
				$ret_data = array(
						'type' => 'success',
						'message' => 'Terima kasih, pesan anda sudah kami terima');
			}else{
				$ret_data = array(
						'type' => 'failed',
						'message' => 'Maaf, pesan anda tidak terkirim. Silakan hubungi kami melalui informasi kontak di samping');
			}
			return $this->output
	            ->set_content_type('application/json')
	            ->set_output(
	            	json_encode($ret_data)
	            );
		}
		show_404();
	}
}
