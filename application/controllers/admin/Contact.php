<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE){
            redirect(base_url('admin/auth/login'));
        }
        $this -> load -> library('form_validation');
        $this->load->model('contact_model');
    }

    public function index()
    {
        $data['contacts'] = $this->contact_model->search(array());
        $this->load->view('admin/contact/list',$data);
    }

    public function create()
    {
        if($this->input->post()){
            $data = array('name' => $this->input->post('name'),
                          'email' => $this->input->post('email'),
                          'message' => $this->input->post('message')
                      );
            $contact_id = $this->contact_model->insert($data);
            redirect(base_url('admin/contact/view/'.$contact_id));
        }
        $this->load->view('admin/contact/create');
    }

    public function edit($id)
    {
        if($this->input->post('delete-confirm')){
            $this->_delete($id);
        }
        $where = array('id' => $id);
        $contact_list = $this->contact_model->search($where,1);
        if(count($contact_list) == 0){
            redirect(base_url('admin/contact/create'));
        }
        if($this->input->post()){
            $data = array('name' => $this->input->post('name'),
                          'email' => $this->input->post('email'),
                          'message' => $this->input->post('message')
                      );
            $this->contact_model->update($data,$id);
            redirect(base_url('admin/contact/view/'.$id));
        }
        $data['contact'] = $contact_list[0];
        $this->load->view('admin/contact/edit',$data);
    }

    public function view($id)
    {
        if($this->input->post('delete-confirm')){
            $this->_delete($id);
        }
        $where = array('id' => $id);
        $contact_list = $this->contact_model->search($where,1);
        if(count($contact_list) == 0){
            redirect(base_url('admin/contact/create'));
        }
        $data['contact'] = $contact_list[0];
        $this->load->view('admin/contact/view',$data);
    }

    function _delete($id)
    {
        $this->contact_model->delete($id);
        $this->session->set_flashdata('info',"Record successfully deleted");
        redirect('admin/contact');
    }

    // public function setting()
    // {
    //     $this->load->view('admin/contact/setting');
    // }
}
