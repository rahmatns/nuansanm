<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this -> load -> library('form_validation');
        if ($this->session->userdata('logged_in') != TRUE){
            redirect(base_url('admin/auth/login'));
        }
        $this->load->model('about_model');
    }

    public function index()
    {
        $about_list = $this->about_model->search(array());
        if(count($about_list) == 0){
            redirect('admin/about/create');
        }
        $data['about'] = $about_list[0];
        $this->load->view('admin/about/index', $data);
    }

    public function create()
    {
        $about_list = $this->about_model->search(array());
        if(count($about_list) > 0){
            redirect('admin/about');
        }
        if($this->input->post()){
            $upload_data = $this->_do_upload();
            if(empty($upload_data['error'])){
                $base_path = str_replace("\\","/",FCPATH);
                $image_path = str_replace($base_path,"",$upload_data['full_path']);
                $data = array(
                    'name' => $this->input->post('name'),
                    'description' => $this->input->post('description'),
                    'image_url' => $image_path
                              );
                $this->about_model->insert($data);
                redirect('admin/about');
            }else{
                $data['error'] = $upload_data['error'];
            }
        }
        $this->load->view('admin/about/create');
    }

    public function edit()
    {
        $about_list = $this->about_model->search(array());
        if(count($about_list) == 0){
            // no record
            redirect('admin/about/create');
        }
        $data['about'] = $about_list[0];
        $id = $data['about']['id'];
        if($this->input->post()){
            if(empty($_FILES['image']['name'])){
                $data = array(
                    'name' => $this->input->post('name'),
                    'description' => $this->input->post('description')
                              );
                $this->about_model->update($data,$id);
                redirect('admin/about');
            }
            $upload_data = $this->_do_upload();
            if(empty($upload_data['error'])){
                $base_path = str_replace("\\","/",FCPATH);
                $image_path = str_replace($base_path,"",$upload_data['full_path']);
                $data = array(
                    'name' => $this->input->post('name'),
                    'description' => $this->input->post('description'),
                    'image_url' => $image_path
                              );
                $this->about_model->update($data,$id);
                redirect('admin/about');
            }else{
                $data['error'] = $upload_data['error'];
            }
        }
        $this->load->view('admin/about/edit',$data);
    }

    function _do_upload(){
        $config['upload_path']          = './assets/images/about/';
        $config['allowed_types']        = 'gif|jpg|png';
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('image')){
            $data = array('error' => $this->upload->display_errors());
            return $data;
        }else{
            return $this->upload->data();
        }
    }
}
