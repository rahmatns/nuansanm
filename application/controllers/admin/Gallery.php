<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE){
            redirect(base_url('admin/auth/login'));
        }
        $this -> load -> library('form_validation');
        $this->load->model('gallery_model');
    }

    public function index()
    {
        $data['galleries'] = $this->gallery_model->search(array());
        $this->load->view('admin/gallery/list',$data);
    }

    public function create()
    {
        $upload_data = array();
        if($this->input->post()){
            $upload_data = $this->_do_upload();
            if(empty($upload_data['error'])){
                $base_path = str_replace("\\","/",FCPATH);
                $image_path = str_replace($base_path,"",$upload_data['full_path']);
                $data = array(
                    'name' => $this->input->post('name'),
                    'image_url' => $image_path
                              );
                $created_id = $this->gallery_model->insert($data);

                redirect('admin/gallery/view/'.$created_id);
            }
        }
        $this->load->view('admin/gallery/create',$upload_data);
    }

    public function edit($id)
    {
        if($this->input->post('delete-confirm')){
            $this->_delete($id);
        }
        $data = array();
        $where = array('id' => $id);
        $galleries = $this->gallery_model->search($where,1);
        if(count($galleries) == 0){
            redirect(base_url('admin/gallery/create'));
        }
        if($this->input->post()){
                if(empty($_FILES['image']['name'])){
                    $data = array(
                        'name' => $this->input->post('name')
                                  );
                    $this->gallery_model->update($data,$id);
                    redirect('admin/gallery/view/'.$id);
                }
                $upload_data = $this->_do_upload();
                if(empty($upload_data['error'])){
                    $base_path = str_replace("\\","/",FCPATH);
                    $image_path = str_replace($base_path,"",$upload_data['full_path']);
                    $data = array(
                        'name' => $this->input->post('name'),
                        'image_url' => $image_path
                                  );
                    $this->gallery_model->update($data,$id);
                    redirect('admin/gallery/view/'.$id);
                }else{
                    $data['error'] = $upload_data['error'];
                }
        }
        $data['gallery'] = $galleries[0];
        $this->load->view('admin/gallery/edit',$data);
    }

    public function view($id)
    {
        if($this->input->post('delete-confirm')){
            $this->_delete($id);
        }
        $where = array('id' => $id);
        $galleries = $this->gallery_model->search($where,1);
        if(count($galleries) == 0){
            redirect(base_url('admin/gallery/create'));
        }
        $data['gallery'] = $galleries[0];
        $this->load->view('admin/gallery/view',$data);
    }

    function _do_upload(){
        $config['upload_path']          = './assets/images/gallery/';
        $config['allowed_types']        = 'gif|jpg|png';
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('image')){
            $data = array('error' => $this->upload->display_errors());
            return $data;
        }else{
            return $this->upload->data();
        }
    }

    function _delete($id)
    {
        $this->gallery_model->delete($id);
        $this->session->set_flashdata('info',"Record successfully deleted");
        redirect('admin/gallery');
    }
}
