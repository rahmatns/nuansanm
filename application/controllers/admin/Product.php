<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE){
            redirect(base_url('admin/auth/login'));
        }
        $this -> load -> library('form_validation');
        $this->load->model('product_model');
    }

    public function index()
    {
        $data['products'] = $this->product_model->search(array());
        $this->load->view('admin/product/list',$data);
    }

    public function create()
    {
        $upload_data = array();
        if($this->input->post()){
            $slugWhere = array('slug' => $this->input->post('slug'));
            $slugMessage = $this->_checkSlug($slugWhere);
            if(empty($slugMessage)){
                $upload_data = $this->_do_upload();
                if(empty($upload_data['error'])){
                    $base_path = str_replace("\\","/",FCPATH);
                    $image_path = str_replace($base_path,"",$upload_data['full_path']);
                    $data = array(
                        'name' => $this->input->post('name'),
                        'description' => $this->input->post('description'),
                        'slug' => $this->input->post('slug'),
                        'image_url' => $image_path
                                  );
                    $created_id = $this->product_model->insert($data);

                    redirect('admin/product/view/'.$created_id);
                }
            }else{
                $upload_data['error'] = $slugMessage;
            }
        }
        $this->load->view('admin/product/create',$upload_data);
    }

    public function edit($id)
    {
        if($this->input->post('delete-confirm')){
            $this->_delete($id);
        }
        $data = array();
        $where = array('id' => $id);
        $products_list = $this->product_model->search($where,1);
        if(count($products_list) == 0){
            redirect(base_url('admin/product/create'));
        }
        if($this->input->post()){
            $slugArray = array('slug' => $this->input->post('slug'),
                               'id !=' => $id);
            $slugMessage = $this->_checkSlug($slugArray);
            if(empty($slugMessage)){
                if(empty($_FILES['image']['name'])){
                    $data = array(
                        'name' => $this->input->post('name'),
                        'description' => $this->input->post('description'),
                        'slug' => $this->input->post('slug')
                                  );
                    $this->product_model->update($data,$id);
                    redirect('admin/product/view/'.$id);
                }
                $upload_data = $this->_do_upload();
                if(empty($upload_data['error'])){
                    $base_path = str_replace("\\","/",FCPATH);
                    $image_path = str_replace($base_path,"",$upload_data['full_path']);
                    $data = array(
                        'name' => $this->input->post('name'),
                        'description' => $this->input->post('description'),
                        'slug' => $this->input->post('slug'),
                        'image_url' => $image_path
                                  );
                    $this->product_model->update($data,$id);
                    redirect('admin/product/view/'.$id);
                }else{
                    $data['error'] = $upload_data['error'];
                }
            }else{
                $data['error'] = $slugMessage;
            }
        }
        $data['product'] = $products_list[0];
        $this->load->view('admin/product/edit',$data);
    }

    public function view($id)
    {
        if($this->input->post('delete-confirm')){
            $this->_delete($id);
        }
        $where = array('id' => $id);
        $products_list = $this->product_model->search($where,1);
        if(count($products_list) == 0){
            redirect(base_url('admin/product/create'));
        }
        $data['product'] = $products_list[0];
        $this->load->view('admin/product/view',$data);
    }

    public function enable($id,$return_to_list=FALSE)
    {
        $where = array('id' => $id);
        $products_list = $this->product_model->search($where,1);
        if(count($products_list) == 0){
            redirect(base_url('admin/product/create'));
        }

        if($this->input->post()){
            $data = array('enable' => 1);
            $this->product_model->update($data,$id);
        }
        if($return_to_list){
            redirect(base_url('admin/homepage/product'));
        }
        redirect(base_url('admin/product/view/'.$id));
    }

    public function disable($id,$return_to_list=FALSE)
    {
        $where = array('id' => $id);
        $products_list = $this->product_model->search($where,1);
        if(count($products_list) == 0){
            redirect(base_url('admin/product/create'));
        }

        if($this->input->post()){
            $data = array('enable' => 0);
            $this->product_model->update($data,$id);
        }
        if($return_to_list){
            redirect(base_url('admin/homepage/product'));
        }
        redirect(base_url('admin/product/view/'.$id));
    }

    function _do_upload(){
        $config['upload_path']          = './assets/images/product/';
        $config['allowed_types']        = 'gif|jpg|png';
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('image')){
            $data = array('error' => $this->upload->display_errors());
            return $data;
        }else{
            return $this->upload->data();
        }
    }

    function _delete($id)
    {
        $this->product_model->delete($id);
        $this->session->set_flashdata('info',"Record successfully deleted");
        redirect('admin/product');
    }

    function _checkSlug($where)
    {
        if(empty($where)){
            return "Slug is required";
        }
        $this->load->model('product_model');
        if ($this->product_model->checkSlug($where))
        {
            return "Slug is exist. Try another slug.";
        } else {
            return false;
        }
    }
}
