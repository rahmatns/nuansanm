<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends CI_Controller {
    protected $data = array();

    public function __construct(){
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE){
            redirect(base_url('admin/auth/login'));
        }

        $this -> load -> library('form_validation');
        $this->load->model('company_model');
        $companies = $this->company_model->search(array());
        if(count($companies) == 0){
            $this->session->set_flashdata('error','Silakan masukkan data perusahaan terlebih dahulu. Pilih menu company.');
            redirect(base_url('admin'));
        }
        $this->data['company'] = $companies[0];
    }

    public function index()
    {
        redirect('admin/homepage/company_view');
    }

    public function company_view()
    {
        $this->data['active'] = 'company';
        $this->load->view('admin/homepage/header',$this->data);
        $this->load->view('admin/homepage/company_view');
        $this->load->view('admin/homepage/footer');
    }

    public function company()
    {
        if($this->input->post()){
            if(empty($_FILES['image']['name'])){
                $data = array(
                    'display_name' => $this->input->post('display_name'),
                    'description' => $this->input->post('description'));
                    $this->company_model->update($data);
                    redirect('admin/homepage/company_view');
            }else{
                $upload_data = $this->_do_upload();
                if(empty($upload_data['error'])){
                    $base_path = str_replace("\\","/",FCPATH);
                    $image_path = str_replace($base_path,"",$upload_data['full_path']);
                    $data = array(
                        'display_name' => $this->input->post('display_name'),
                        'description' => $this->input->post('description'),
                        'image_url' => $image_path
                                  );
                    $this->company_model->update($data);
                    redirect('admin/homepage/company_view');
                }else{
                    $this->session->set_flashdata('error',$upload_data['error']);
                }
            }

        }
        $this->load->view('admin/homepage/company',$this->data);
    }

    public function slideshow()
    {
        $this->load->model('slideshow_model');
        $order_by = "enable desc, sequence asc";
        $this->data['slideshow_list'] = $this->slideshow_model->search(array(),FALSE,$order_by);
        $this->load->view('admin/homepage/slideshow',$this->data);
    }

    public function product()
    {
        $this->load->model('product_model');
        $order_by = "enable desc";
        $this->data['products'] = $this->product_model->search(array(),FALSE,$order_by);
        $this->load->view('admin/homepage/product',$this->data);
    }

    public function testimony()
    {
        $this->load->model('testimony_model');
        $this->data['testimonies'] = $this->testimony_model->search(array());
        $this->load->view('admin/homepage/testimony',$this->data);
    }

    public function contact()
    {
        $this->load->view('admin/homepage/contact',$this->data);
    }

    public function contact_view()
    {
        $this->load->view('admin/homepage/contact',$this->data);
    }

    function _do_upload(){
        $config['upload_path']          = './assets/images/homepage/';
        $config['allowed_types']        = 'gif|jpg|png';
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('image')){
            $data = array('error' => $this->upload->display_errors());
            return $data;
        }else{
            return $this->upload->data();
        }
    }
}
