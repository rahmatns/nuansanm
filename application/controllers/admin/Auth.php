<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct(){
        parent::__construct();
    }

    public function login()
    {
        if ($this->input->post()){
            $this->load->model('auth_model');

            $email = $this->input->post('email');
            $pass = $this->input->post('password');
            $hash = md5($pass);

            $where = array('email' => $email,
                           'hash' => $hash);
            $users = $this->auth_model->search($where,1);
            if(count($users) > 0) {
                print_r(json_encode($users[0]));
                $this->session->set_userdata('logged_in',TRUE);
                $this->session->set_userdata('user_role',$users[0]['role']);
                $this->session->set_userdata('user_id',$users[0]['id']);
                $this->session->set_userdata('user_name',$users[0]['name']);
                redirect(base_url('admin'));
            } else{
                $this->session->set_flashdata('error',"User not found or password is incorrect!");
            }

        }
        $this->load->view('admin/login');
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url('admin/auth/login'));
    }
}
