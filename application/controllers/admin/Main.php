<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this -> load -> library('form_validation');
        if ($this->session->userdata('logged_in') != TRUE){
            redirect(base_url('admin/auth/login'));
        }
    }

    public function index()
    {
        $this->load->view('admin/index');
    }

    public function homepage()
    {
        $this->load->view('admin/homepage');
    }

    public function about()
    {
        $this->load->view('admin/about');
    }

    public function contact()
    {
        $this->load->view('admin/contact-us');
    }
}
