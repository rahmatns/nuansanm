<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE){
            redirect(base_url('admin/auth/login'));
        }

        $this -> load -> library('form_validation');
        $this->load->model('company_model');
    }

    public function index()
    {
        $data['companies'] = $this->company_model->search(array());
        $this->load->view('admin/company/list',$data);
    }

    public function create()
    {
        $companies = $this->company_model->search(array());
        if(count($companies) > 0){
            redirect(base_url('admin/company'));
        }
        if($this->input->post()){
            $data = array(
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'address' => $this->input->post('address'),
            );
            $created_id = $this->company_model->insert($data);
            redirect('admin/company/view/'.$created_id);
        }
        $this->load->view('admin/company/create');
    }


    public function view($id)
    {
        $where = array('id' => $id);
        $data['company'] = $this->company_model->search($where,1)[0];
        $this->load->view('admin/company/view',$data);
    }

    public function edit($id)
    {
        if($this->input->post()){
            $data = array(
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'address' => $this->input->post('address'),
            );
            $this->company_model->update($data,$id);
            redirect('admin/company/view/'.$id);
        }
        $where = array('id' => $id);
        $data['company'] = $this->company_model->search($where,1)[0];
        $this->load->view('admin/company/edit',$data);
    }
}
