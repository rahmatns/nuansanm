<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slideshow extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE){
            redirect(base_url('admin/auth/login'));
        }
        $this -> load -> library('form_validation');
        $this->load->helper('form');
        $this->load->model('slideshow_model');
    }

    public function index()
    {
        $this->create();
    }

    public function create()
    {
        $upload_data = array();
        if($this->input->post()){
            $upload_data = $this->_do_upload();
            if(empty($upload_data['error'])){
                $base_path = str_replace("\\","/",FCPATH);
                $image_path = str_replace($base_path,"",$upload_data['full_path']);
                $data = array(
                    'name' => $this->input->post('name'),
                    'description' => $this->input->post('description'),
                    'sequence' => $this->input->post('sequence'),
                    'image_url' => $image_path
                              );
                $created_id = $this->slideshow_model->insert($data);

                redirect('admin/slideshow/view/'.$created_id);
            }
        }
        $this->load->view('admin/slideshow/create',$upload_data);
    }

    public function edit($id)
    {
        if($this->input->post('delete-confirm')){
            $this->_delete($id);
        }
        $data = array();
        $where = array('id' => $id);
        $slideshow_list = $this->slideshow_model->search($where,1);
        if(count($slideshow_list) == 0){
            redirect(base_url('admin/slideshow/create'));
        }
        if($this->input->post()){
            if(empty($_FILES['image']['name'])){
                $data = array(
                    'name' => $this->input->post('name'),
                    'description' => $this->input->post('description'),
                    'sequence' => $this->input->post('sequence')
                              );
                $this->slideshow_model->update($data,$id);
                redirect('admin/slideshow/view/'.$id);
            }
            $upload_data = $this->_do_upload();
            if(empty($upload_data['error'])){
                $base_path = str_replace("\\","/",FCPATH);
                $image_path = str_replace($base_path,"",$upload_data['full_path']);
                $data = array(
                    'name' => $this->input->post('name'),
                    'description' => $this->input->post('description'),
                    'sequence' => $this->input->post('sequence'),
                    'image_url' => $image_path
                              );
                $this->slideshow_model->update($data,$id);
                redirect('admin/slideshow/view/'.$id);
            }else{
                $data['error'] = $upload_data['error'];
            }
        }
        $data['slideshow'] = $slideshow_list[0];
        $this->load->view('admin/slideshow/edit',$data);
    }

    public function view($id)
    {
        if($this->input->post('delete-confirm')){
            $this->_delete($id);
        }
        $where = array('id' => $id);
        $slideshow_list = $this->slideshow_model->search($where,1);
        if(count($slideshow_list) == 0){
            redirect(base_url('admin/slideshow/create'));
        }
        $data['slideshow'] = $slideshow_list[0];
        $this->load->view('admin/slideshow/view',$data);
    }

    public function enable($id,$return_to_list=FALSE)
    {
        $where = array('id' => $id);
        $slideshow_list = $this->slideshow_model->search($where,1);
        if(count($slideshow_list) == 0){
            redirect(base_url('admin/slideshow/create'));
        }

        if($this->input->post()){
            $data = array('enable' => 1);
            $this->slideshow_model->update($data,$id);
        }
        if($return_to_list){
            redirect(base_url('admin/homepage/slideshow'));
        }
        redirect(base_url('admin/slideshow/view/'.$id));
    }

    public function disable($id,$return_to_list=FALSE)
    {
        $where = array('id' => $id);
        $slideshow_list = $this->slideshow_model->search($where,1);
        if(count($slideshow_list) == 0){
            redirect(base_url('admin/slideshow/create'));
        }

        if($this->input->post()){
            $data = array('enable' => 0);
            $this->slideshow_model->update($data,$id);
        }
        if($return_to_list){
            redirect(base_url('admin/homepage/slideshow'));
        }
        redirect(base_url('admin/slideshow/view/'.$id));
    }

    function _do_upload(){
        $config['upload_path']          = './assets/images/homepage/';
        $config['allowed_types']        = 'gif|jpg|png';
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('image')){
            $data = array('error' => $this->upload->display_errors());
            return $data;
        }else{
            return $this->upload->data();
        }
    }

    function _delete($id)
    {
        $this->slideshow_model->delete($id);
        $this->session->set_flashdata('info',"Record successfully deleted");
        redirect('admin/homepage/slideshow');
    }
}
