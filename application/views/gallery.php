<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>PT. Nuansa Nisa Met &mdash; Jasa Bodem dan Roll Plat: Menerima pengerjaan Pembuatan tangki, Bodem, dan Roll Plat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="PT. Nuansa Nisa Met &mdash; Jasa Bodem dan Roll Plat: Menerima pengerjaan Pembuatan tangki, Bodem, dan Roll Plat" />
    <meta name="keywords" content="Nuansa Nisa Met, Tangki, Bodem, Roll Plat" />
    <meta name="author" content="PT. Nuansa Nisa Met" />

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet">

    <!-- Animate.css -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/magnific-popup.css">
    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/owl.theme.default.min.css">
    <!-- Flexslider  -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/flexslider.css">
    <!-- Theme style  -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/style.css">

    <!-- Modernizr JS -->
    <script src="<?=base_url()?>assets/js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="<?=base_url()?>assets/js/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
    .gallery-item {
        margin-bottom: 15px;
        padding: 0 8px;
        overflow-y: hidden;
        height: 250px;
        float: left;
        width: 33.333333333%;
        transition: 0.3s
    }

    /* Create four equal columns that floats next to eachother */
    .gallery-item:hover {
        cursor: pointer;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)
    }
    </style>
    <link rel="stylesheet" href="<?=base_url('assets/halkaBox')?>/halkaBox.min.css">
    <script src="<?=base_url('assets/halkaBox')?>/halkaBox.min.js"></script>
    </head>
    <?php
    $about_img_url = base_url('assets/images/blog-1.jpg');
    ?>
    <body>
    <div id="page">
    <?php
    $data['active'] = 'gallery';
    $this->load->view('frontend-menu',$data);
    ?>
    <div class="container">
        <div class="row" id="gallery-thumb-container">
            <?php foreach ($galleries as $gallery): ?>
                <a href="<?=base_url($gallery['image_url'])?>" class="gallery-item gallery1" title="<?=$gallery['name']?>">
                    <img class="img img-responsive" src="<?=base_url($gallery['image_url'])?>"/>
                </a>
            <?php endforeach ?>
        </div>
    </div>
    <script type="text/javascript">
        halkaBox.run("gallery1");
    </script>