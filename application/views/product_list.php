<div class="container">
    <div class="row">
        <div class="intro animate-box">
            <?php if (count($product_list) == 1): ?>
                <div class="col-md-4 col-sm-3"></div>
            <?php endif ?>
            <?php if (count($product_list) == 2): ?>
                <div class="col-md-3"></div>
            <?php endif ?>
            <?php if (count($product_list) == 3): ?>
                <div class="col-md-2 col-xs-12 col-sm-3"></div>
            <?php endif ?>
            <?php
                foreach ($product_list as $product): ?>
                <?php
                    $product['link'] = $product['id']."-".$product['slug'];
                ?>
                <div class="col-md-3 col-xs-12 col-sm-6">
                    <a class="product-item-card" href="<?=base_url('main/product/'.$product['link'])?>">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <center>
                                    <img class="img img-responsive" src="<?=base_url($product['image_url'])?>" />
                                </center>
                            </div>
                            <div class="panel-body">
                                <h3 class="text-center">
                                    <?=$product['name']?>
                                </h3>
                                <button class="btn btn-success btn-block">More Detail</button>
                            </div>
                        </div>
                    </a>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>