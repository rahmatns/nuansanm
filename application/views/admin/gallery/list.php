<?php
$this->load->view('admin/gallery/header');
?>
<div id="bread-crumb">
    <a class="active">Gallery</a>
    <div>
        <a href="<?=base_url('admin/gallery/create')?>" class="btn btn-default">Create</a>
    </div>
</div>
<div id="content">
    <?php if (!isset($galleries) || count($galleries) == 0): ?>
        Belum ada gallery.
    <?php else: ?>
    <div class="table-responsive">
      <table class="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Caption</th>
            <th>Gambar</th>
            <th/>
          </tr>
        </thead>
        <tbody>
        <?php
        $counter = 1;
        foreach ($galleries as $gallery): ?>
          <tr>
            <td><?=$counter++?></td>
            <td><?=$gallery['name']?></td>
            <td>
                <img src="<?=base_url($gallery['image_url'])?>" width="300px" height="100px">
            </td>
            <td><a href="<?=base_url('admin/gallery/view/'.$gallery['id'])?>" class="btn btn-sm btn-info">Lihat</a></td>
          </tr>
        <?php endforeach ?>
        </tbody>
      </table>
    </div>
    <?php endif ?>
</div>
<?php
$this->load->view('admin/gallery/footer');
?>