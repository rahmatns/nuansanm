<?php
$this->load->view('admin/gallery/header');
?>
<div id="bread-crumb">
    <a href="<?=base_url('admin/gallery')?>">Gallery</a> /
    <a class="active"> New</a>
</div>
<div id="content-detail">
    <?php if (isset($error)): ?>
        <div class="row">
            <div class="alert alert-danger">
                <span class="text-center"><?=$error?></span>
            </div>
        </div>
    <?php endif ?>
    <?php echo form_open_multipart(site_url('admin/gallery/create'));?>
        <div class="form-group">
            <label for="image" class="control-label">Gambar</label>
            <input type="file" class="form-control" name="image">
        </div>
        <div class="form-group">
            <label for="title" class="control-label">Caption (Optional)</label>
            <input type="text" class="form-control" name="name" value="<?=set_value('name')?>"
             placeholder="Judul...">
        </div>
        <div class="form-group pull-right">
            <a href="<?=base_url('admin/product')?>" class="btn btn-warning">Cancel</a>
            <button type="submit" class="btn btn-primary">Create</button>
        </div>
    </form>
</div>
<?php
$this->load->view('admin/gallery/footer');
?>
