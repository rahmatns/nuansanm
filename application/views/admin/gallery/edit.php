<?php
$this->load->view('admin/gallery/header');
?>
<div id="bread-crumb">
    <a href="<?=base_url('admin/gallery')?>">Gallery</a> /
    <a class="active"> <?=empty($gallery['name']) ? $gallery['id'] : $gallery['name']?></a>
</div>
<div class="action-container">
    <div class="action-center">
        <div class="dropdown" title="Click to show action">
          <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Action
          <span class="caret"></span></button>
          <ul class="dropdown-menu action-menu">
            <li><a href="#deleteModal" data-toggle="modal">Delete</a></li>
          </ul>
        </div>
    </div>
</div>
<div id="content-detail">
    <?php if (isset($error)): ?>
        <div class="row">
            <div class="alert alert-danger">
                <span class="text-center"><?=$error?></span>
            </div>
        </div>
    <?php endif ?>
    <?php echo form_open_multipart();?>
        <div class="form-group">
            <label for="image" class="control-label">Gambar</label><br/>
            <img src="<?=base_url($gallery['image_url'])?>" alt="<?=$gallery['name']?>" class="img img-responsive">
            <input type="file" class="form-control" name="image">
        </div>
        <div class="form-group">
            <label for="name" class="control-label">Caption (Optional)</label>
            <input type="text" class="form-control" name="name" value="<?php echo isset($gallery['name']) ? $gallery['name'] : set_value('name'); ?>"
             placeholder="Judul..."/>
        </div>
        <div class="form-group pull-right">
            <a href="<?=base_url('admin/gallery/view/'.$gallery['id'])?>" class="btn btn-warning">Cancel</a>
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
    </form>
</div>
<?php
$this->load->view('admin/gallery/footer');
?>
