<?php
$this->load->view('admin/gallery/header');
?>
<div id="bread-crumb">
    <a href="<?=base_url('admin/gallery')?>">Gallery</a> /
    <a class="active"> <?=empty($gallery['name']) ? $gallery['id'] : $gallery['name']?></a>
</div>
<div class="action-container">
    <div class="action-center">
        <div class="dropdown" title="Click to show action">
          <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Action
          <span class="caret"></span></button>
          <ul class="dropdown-menu action-menu">
            <li><a href="#deleteModal" data-toggle="modal">Delete</a></li>
          </ul>
        </div>
    </div>
</div>
<div id="content-detail">
        <div class="form-group">
            <label for="gambar" class="control-label">Gambar</label><br/>
            <img src="<?=base_url($gallery['image_url'])?>" alt="<?=$gallery['name']?>" class="img img-responsive">
        </div>
        <div class="form-group">
            <label for="name" class="control-label">Caption</label>
            <input type="text" class="form-control" name="name" value="<?php echo isset($gallery['name']) ? $gallery['name'] : set_value('name'); ?>"
             placeholder="Judul..." readonly="readonly" />
        </div>
        <div class="form-group pull-right">
            <a href="<?=base_url('admin/gallery/edit/'.$gallery['id'])?>" class="btn btn-warning">Edit</a>
        </div>
</div>
<?php
$this->load->view('admin/gallery/footer');
?>
