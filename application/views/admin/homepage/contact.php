<?php
$data['active'] = 'contact';
$this->load->view('admin/homepage/header',$data);
?>

        <div id="content-detail">
            <h3 class="text-center">Pengaturan Homepage</h3>
            <form method="post" action="<?php echo site_url('admin/main/company');?>">
                <div class="form-group">
                    <label for="name" class="control-label">Logo Perusahaan</label>
                    <input type="hidden" class="form-control" name="user_id" value="<?php echo isset($user->user_id) ? $user->user_id : set_value('user_id'); ?>">
                    <input type="text" class="form-control" name="name" value="<?php echo isset($user->name) ? $user->name : set_value('name'); ?>" placeholder="Nama Perusahaan..." >
                </div>
                <div class="form-group">
                    <label for="emailx" class="control-label">Judul Perusahaan</label>
                    <input type="text" class="form-control" value="<?php echo isset($user->email) ? $user->email : set_value('email'); ?>" name="emailx" placeholder="Alamat..." >
                </div>
                <div class="form-group">
                    <label for="emailx" class="control-label">Subjudul Perusahaan</label>
                    <input type="text" class="form-control" value="<?php echo isset($user->email) ? $user->email : set_value('email'); ?>" name="emailx" placeholder="Email..." >
                </div>
                <div class="form-group">
                    <label for="emailx" class="control-label">Slide Shoe</label>
                    <input type="text" class="form-control" value="<?php echo isset($user->email) ? $user->email : set_value('email'); ?>" name="emailx" placeholder="Slide..." >
                </div>
                <div class="form-group">
                    <label for="emailx" class="control-label">Produk yang ditampilkan</label>
                    <input type="text" class="form-control" value="<?php echo isset($user->email) ? $user->email : set_value('email'); ?>" name="emailx" placeholder="No Telepon..." >
                </div>
                <div class="form-group">
                    <label for="emailx" class="control-label">Testimoni yang ditampilkan</label>
                    <input type="text" class="form-control" value="<?php echo isset($user->email) ? $user->email : set_value('email'); ?>" name="emailx" placeholder="No Telepon..." >
                </div>
                <div class="form-group">
                     <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                    <button class="btn btn-primary pull-right">Update</button>
                </div>
            </form>
        </div>
        <?php
        $this->load->view('admin/homepage/footer');
        ?>