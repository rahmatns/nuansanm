<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>PT. Nuansa Nisa Met &mdash; Jasa Bodem dan Roll Plat: Menerima pengerjaan Pembuatan tangki, Bodem, dan Roll Plat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet">

    <!-- Bootstrap  -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.css">
    <!-- Backend  -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/backend.css">
    </head>
    <body>

        <nav class="navbar navbar-default">
                    <?php
                    if(empty($active)){
                        $active = 'none';
                    }

                    $data['menu'] = 'Homepage';
                    $this->load->view('admin/base-menu.php',$data);
                    ?>
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav navbar-right">
                <li class="<?=$active == 'company' ? 'active' : ''?>">
                    <a href="<?=base_url('admin/homepage/company_view')?>">Company</a>
                </li>
                <li class="<?=$active == 'slideshow' ? 'active' : ''?>">
                    <a href="<?=base_url('admin/homepage/slideshow')?>">Slideshow</a>
                </li>
                <li class="<?=$active == 'product' ? 'active' : ''?>">
                    <a href="<?=base_url('admin/homepage/product')?>">Product</a>
                </li>
                <li class="<?=$active == 'testimony' ? 'active' : ''?>">
                    <a href="<?=base_url('admin/homepage/testimony')?>">Testimony</a>
                </li>
<!--                 <li class="<?=$active == 'contact' ? 'active' : ''?>">
                    <a href="<?=base_url('admin/homepage/contact')?>">Kontak Kami</a>
                </li> -->
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
    <div id="page">