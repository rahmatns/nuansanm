<div id="content-detail">
    <h3 class="text-center">Pengaturan Homepage</h3>
        <div class="form-group">
            <label for="image" class="control-label">Logo Perusahaan</label><br/>
            <img src="<?=base_url($company['image_url'])?>">
        </div>
        <div class="form-group">
            <label for="display_name" class="control-label">Nama Perusahaan yang Ditampilkan</label>
            <input type="text" class="form-control" value="<?=$company['display_name']?>" name="display_name" placeholder="Nama perusahaan yang ditampilkan..." readonly="readonly">
        </div>
        <div class="form-group">
            <label for="description" class="control-label">Keterangan Perusahaan</label>
            <input type="text" class="form-control" value="<?=$company['description']?>" name="description" placeholder="Keterangan Perusahaan..." readonly="readonly" >
        </div>
        <div class="form-group pull-right">
            <a href="<?=base_url('admin/homepage/company')?>" class="btn btn-warning">Edit</a>
        </div>
</div>