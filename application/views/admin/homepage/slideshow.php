<?php
$data['active'] = 'slideshow';
$this->load->view('admin/homepage/header',$data);
?>
        <style type="text/css">
            #content{
                padding: 0px 15px;
            }
        </style>

        <div id="bread-crumb">
            <a href="<?=base_url('admin/homepage')?>">Homepage</a> /
            <a class="active"> Slideshow</a>
            <div>
                <a href="<?=base_url('admin/slideshow/create')?>" class="btn btn-default">Create</a>
            </div>
        </div>
        <div id="content">
            <?php if (!isset($slideshow_list) || count($slideshow_list) == 0): ?>
                Belum ada slideshow.
            <?php else: ?>
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Sequence</th>
                    <th>Nama</th>
                    <th>Deskripsi</th>
                    <th>Gambar</th>
                    <th title="Enable in homepage?">Enable ?</th>
                    <th/>
                  </tr>
                </thead>
                <tbody>
                <?php
                $counter = 1;
                foreach ($slideshow_list as $slideshow): ?>
                  <tr>
                    <td><?=$counter++?></td>
                    <td><?=$slideshow['sequence']?></td>
                    <td><?=$slideshow['name']?></td>
                    <td><?=$slideshow['description']?></td>
                    <td>
                        <img src="<?=base_url($slideshow['image_url'])?>" width="100px" height="100px">
                    </td>
                    <td>
                        <?php if ($slideshow['enable']): ?>
                            <div style="display: inline-flex;">
                                <form method="post" action="<?=base_url('admin/slideshow/disable/'.$slideshow['id'].'/list')?>">
                                    <input type="hidden" name="enable">
                                    <button class="btn btn-sm btn-success" type="submit" title="Disable from homepage ?">✓</button>
                                </form>
                            </div>
                        <?php else: ?>
                            <form method="post" action="<?=base_url('admin/slideshow/enable/'.$slideshow['id'].'/list')?>">
                                <input type="hidden" name="enable">
                                <button class="btn btn-sm btn-warning" type="submit" title="Show in homepage ?">X</button>
                            </form>
                        <?php endif ?>
                    </td>
                    <td><a href="<?=base_url('admin/slideshow/view/'.$slideshow['id'])?>" class="btn btn-sm btn-info">Lihat</a></td>
                  </tr>
                <?php endforeach ?>
                </tbody>
              </table>
            </div>
            <?php endif ?>
        </div>
        <?php
        $this->load->view('admin/homepage/footer');
        ?>