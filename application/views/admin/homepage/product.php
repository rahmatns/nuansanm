<?php
$data['active'] = 'product';
$this->load->view('admin/homepage/header',$data);
?>
        <style type="text/css">
            #content{
                padding: 0px 15px;
            }
        </style>

        <div id="bread-crumb">
            <a href="<?=base_url('admin/homepage')?>">Homepage</a> /
            <a class="active"> Product</a>
            <div>
                <a href="<?=base_url('admin/product/create')?>" class="btn btn-default">Create</a>
            </div>
        </div>
        <div id="content">
            <?php if (!isset($products) || count($products) == 0): ?>
                Belum ada product.
            <?php else: ?>
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Judul</th>
                    <th>Slug</th>
                    <th>Deskripsi</th>
                    <th>Gambar</th>
                    <th title="Enable in homepage?">Enable ?</th>
                    <th/>
                  </tr>
                </thead>
                <tbody>
                <?php
                $counter = 1;
                foreach ($products as $product): ?>
                  <tr>
                    <td><?=$counter++?></td>
                    <td><?=$product['name']?></td>
                    <td><?=$product['slug']?></td>
                    <td><?=$product['description']?></td>
                    <td>
                        <img src="<?=base_url($product['image_url'])?>" width="100px" height="100px">
                    </td>
                    <td>
                        <?php if ($product['enable']): ?>
                            <div style="display: inline-flex;">
                                <form method="post" action="<?=base_url('admin/product/disable/'.$product['id'].'/list')?>">
                                    <input type="hidden" name="enable">
                                    <button class="btn btn-sm btn-success" type="submit" title="Disable from homepage ?">✓</button>
                                </form>
                            </div>
                        <?php else: ?>
                            <form method="post" action="<?=base_url('admin/product/enable/'.$product['id'].'/list')?>">
                                <input type="hidden" name="enable">
                                <button class="btn btn-sm btn-warning" type="submit" title="Show in homepage ?">X</button>
                            </form>
                        <?php endif ?>
                    </td>
                    <td><a href="<?=base_url('admin/product/view/'.$product['id'])?>" class="btn btn-sm btn-info">Lihat</a></td>
                  </tr>
                <?php endforeach ?>
                </tbody>
              </table>
            </div>
            <?php endif ?>
        </div>
        <?php
        $this->load->view('admin/homepage/footer');
        ?>