<?php
$data['active'] = 'company';
$this->load->view('admin/homepage/header',$data);
?>
        <?php
        $default_display_name = empty($company['display_name']) ? $company['name'] : $company['display_name'];
        $default_description = empty($company['description']) ? $company['address'] : $company['description'];
        ?>
        <div id="content-detail">
            <?php if ($this->session->flashdata('error')): ?>
                <div class="alert alert-danger">
                  <span><?=$this->session->flashdata('error')?></span>
                </div>
            <?php endif ?>
            <h3 class="text-center">Pengaturan Homepage</h3>
            <?php echo form_open_multipart(site_url('admin/homepage/company'));?>
                <div class="form-group">
                    <label for="image" class="control-label">Logo Perusahaan</label><br/>
                    <img src="<?=base_url($company['image_url'])?>"><br/>
                    <input type="file" class="form-control" name="image">
                </div>
                <div class="form-group">
                    <label for="display_name" class="control-label">Nama Perusahaan yang Ditampilkan</label>
                    <input type="text" class="form-control" value="<?=set_value('display_name',$default_display_name)?>" name="display_name" placeholder="Nama perusahaan yang ditampilkan...">
                </div>
                <div class="form-group">
                    <label for="description" class="control-label">Keterangan Perusahaan</label>
                    <input type="text" class="form-control" value="<?=set_value('description',$default_description)?>" name="description" placeholder="Keterangan Perusahaan..." >
                </div>
                <div class="form-group pull-right">
                    <a href="<?=base_url('admin/homepage/company_view')?>" class="btn btn-warning">Cancel</a>
                    <button type="submit" class="btn btn-success">Update</button>
                </div>
            </form>
        </div>
        <?php
        $this->load->view('admin/homepage/footer');
        ?>