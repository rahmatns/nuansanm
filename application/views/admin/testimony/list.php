<?php
$this->load->view('admin/testimony/header');
?>
<div id="bread-crumb">
    <a class="active">Testimony</a>
    <div>
        <a href="<?=base_url('admin/testimony/create')?>" class="btn btn-default">Create</a>
    </div>
</div>
<div id="content">
    <?php if (!isset($testimonies) || count($testimonies) == 0): ?>
        Belum ada testimony.
    <?php else: ?>
    <div class="table-responsive">
      <table class="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Nama</th>
            <th>Deskripsi</th>
            <th>Sequence</th>
            <th>Gambar</th>
            <th>Enable ?</th>
            <th/>
          </tr>
        </thead>
        <tbody>
        <?php
        $counter = 1;
        foreach ($testimonies as $testimony): ?>
          <tr>
            <td><?=$counter++?></td>
            <td><?=$testimony['name']?></td>
            <td><?=$testimony['description']?></td>
            <td><?=$testimony['sequence']?></td>
            <td>
                <img src="<?=base_url($testimony['image_url'])?>" width="100px" height="100px">
            </td>
            <td>
                <?php if ($testimony['enable']): ?>
                    <div style="display: inline-flex;">
                        <form method="post" action="<?=base_url('admin/testimony/disable/'.$testimony['id'].'/list')?>">
                            <input type="hidden" name="enable">
                            <button class="btn btn-sm btn-success" type="submit" title="Disable from homepage ?">✓</button>
                        </form>
                    </div>
                <?php else: ?>
                    <form method="post" action="<?=base_url('admin/testimony/enable/'.$testimony['id'].'/list')?>">
                        <input type="hidden" name="enable">
                        <button class="btn btn-sm btn-warning" type="submit" title="Show in homepage ?">X</button>
                    </form>
                <?php endif ?>
            </td>
            <td><a href="<?=base_url('admin/testimony/view/'.$testimony['id'])?>" class="btn btn-sm btn-info">Lihat</a></td>
          </tr>
        <?php endforeach ?>
        </tbody>
      </table>
    </div>
    <?php endif ?>
</div>
<?php
$this->load->view('admin/testimony/footer');
?>