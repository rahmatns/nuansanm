<?php
$this->load->view('admin/testimony/header');
?>
<div id="bread-crumb">
    <a href="<?=base_url('admin/testimony')?>">Testimony</a> /
    <a class="active"> New</a>
</div>
<div id="content-detail">
    <?php if (isset($error)): ?>
        <div class="row">
            <div class="alert alert-danger">
                <span class="text-center"><?=$error?></span>
            </div>
        </div>
    <?php endif ?>
    <?php echo form_open_multipart(site_url('admin/testimony/create'));?>
        <div class="form-group">
            <label for="name" class="control-label">Nama</label>
            <input type="text" class="form-control" name="name" value="<?=set_value('name')?>"
             placeholder="Nama..." required="required" >
        </div>
        <div class="form-group">
            <label for="image" class="control-label">Gambar</label>
            <input type="file" class="form-control" name="image">
        </div>
        <div class="form-group">
            <label for="description" class="control-label">Deskripsi</label>
            <input type="text" class="form-control" value="<?=set_value('description')?>" name="description" placeholder="Deskripsi..." >
        </div>
        <div class="form-group">
            <label for="sequence" class="control-label">Sequence</label>
            <input type="number" class="form-control" value="<?=set_value('sequence')?>" name="sequence">
        </div>
        <div class="form-group pull-right">
            <a href="<?=base_url('admin/testimony')?>" class="btn btn-warning">Cancel</a>
            <button class="btn btn-primary">Create</button>
        </div>
    </form>
</div>
<?php
$this->load->view('admin/testimony/footer');
?>
