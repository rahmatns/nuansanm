<?php
$this->load->view('admin/testimony/header');
?>
<div id="bread-crumb">
    <a href="<?=base_url('admin/testimony')?>">Testimony</a> /
    <a class="active"> <?=$testimony['name']?></a>
</div>
<div class="action-container">
    <div class="action-left">
        <?php if ($testimony['enable'] == TRUE): ?>
            <form method="post" action="<?=base_url('admin/testimony/disable/'.$testimony['id'])?>">
                <input type="hidden" name="enable">
                <button class="btn btn-default" type="submit" title="Disable from homepage ?">REMOVE FROM HOMEPAGE</button>
            </form>
        <?php else: ?>
            <form method="post" action="<?=base_url('admin/testimony/enable/'.$testimony['id'])?>">
                <input type="hidden" name="enable">
                <button class="btn btn-default" type="submit" title="Show in homepage ?">SHOW IN HOMEPAGE</button>
            </form>
        <?php endif ?>
    </div>
    <div class="action-center">
        <div class="dropdown">
          <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Action
          <span class="caret"></span></button>
          <ul class="dropdown-menu action-menu">
            <li><a href="#deleteModal" data-toggle="modal">Delete</a></li>
          </ul>
        </div>
    </div>
    <div class="action-right"></div>
</div>
<div id="content-detail">
        <div class="form-group">
            <label for="name" class="control-label">Nama</label>
            <input type="text" class="form-control" name="name" value="<?=$testimony['name']?>"
             placeholder="Nama..." readonly="readonly" />
        </div>
        <div class="form-group">
            <label class="control-label">Gambar</label><br/>
            <img src="<?=base_url($testimony['image_url'])?>" alt="<?=$testimony['name']?>" width="100px" height="100px">
        </div>
        <div class="form-group">
            <label for="description" class="control-label">Deskripsi</label>
            <input type="text" class="form-control" name="description" value="<?=$testimony['description']?>" readonly="readonly" />
        </div>
        <div class="form-group">
            <label for="sequence" class="control-label">Sequence</label>
            <input type="number" class="form-control" value="<?=$testimony['sequence']?>" name="sequence" readonly="readonly" >
        </div>
        <div class="form-group pull-right">
            <a href="<?=base_url('admin/testimony/edit/'.$testimony['id'])?>" class="btn btn-warning">Edit</a>
        </div>
</div>
<?php
$this->load->view('admin/testimony/footer');
?>
