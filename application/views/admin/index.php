<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>PT. Nuansa Nisa Met &mdash; Jasa Bodem dan Roll Plat: Menerima pengerjaan Pembuatan tangki, Bodem, dan Roll Plat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.css">
    <!-- Backend  -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/backend.css">

    <style type="text/css">
        #page{
            padding: 20px;
            min-height: 100vh;
            min-width: 100vw;
            background: linear-gradient(to bottom right, #fdf431, #3e7b00);
        }
    </style>
    </head>
    <body>
    <div id="page">
        <?php if ($this->session->flashdata('error')): ?>
            <div class="alert alert-danger">
              <span><?=$this->session->flashdata('error')?></span>
            </div>
        <?php endif ?>
        <nav class="navbar">
            <div class="container-fluid">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <button href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button">
                            <?php
                            if(!empty($this->session->userdata('user_name'))){
                                echo "Halo, ".$this->session->userdata('user_name')." ";
                            }
                            ?>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="<?=base_url('admin/auth/logout')?>">Keluar</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="row">
            <div class="menu-list">
                <div class="menu-item">
                    <a href="<?=base_url('admin/company')?>">
                        <img src="<?=base_url('icons/menuitem_company.png')?>" class="img-responsive">
                        <span class="caption">Company</span>
                    </a>
                </div>
                <div class="menu-item">
                    <a href="<?=base_url('admin/homepage')?>">
                        <img src="<?=base_url('icons/menuitem_noicon.png')?>" class="img-responsive">
                        <span class="caption">Homepage</span>
                    </a>
                </div>
                <div class="menu-item">
                    <a href="<?=base_url('admin/about')?>">
                        <img src="<?=base_url('icons/menuitem_noiconi.png')?>" class="img-responsive">
                        <span class="caption">About Us</span>
                    </a>
                </div>
                <div class="menu-item">
                    <a href="<?=base_url('admin/product')?>">
                        <img src="<?=base_url('icons/menuitem_noiconii.png')?>" class="img-responsive">
                        <span class="caption">Product</span>
                    </a>
                </div>
                <div class="menu-item">
                    <a href="<?=base_url('admin/gallery')?>">
                        <img src="<?=base_url('icons/menuitem_gallery.png')?>" class="img-responsive">
                        <span class="caption">Gallery</span>
                    </a>
                </div>
                <div class="menu-item">
                    <a href="<?=base_url('admin/testimony')?>">
                        <img src="<?=base_url('icons/menuitem_noiconiii.png')?>" class="img-responsive">
                        <span class="caption">Testimony</span>
                    </a>
                </div>
                <div class="menu-item">
                    <a href="<?=base_url('admin/contact')?>">
                        <img src="<?=base_url('icons/menuitem_noiconiv.png')?>" class="img-responsive">
                        <span class="caption">Contact</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    </body>

    <!-- Javascript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
</html>

