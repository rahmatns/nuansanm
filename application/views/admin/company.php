<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>PT. Nuansa Nisa Met &mdash; Jasa Bodem dan Roll Plat: Menerima pengerjaan Pembuatan tangki, Bodem, dan Roll Plat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet">

    <!-- Animate.css -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/magnific-popup.css">
    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/owl.theme.default.min.css">
    <!-- Flexslider  -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/flexslider.css">
    <!-- Theme style  -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/style.css">

    <!-- Modernizr JS -->
    <script src="<?=base_url()?>assets/js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="<?=base_url()?>assets/js/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .qbootstrap-nav ul li a,.qbootstrap-nav ul li.active > a,.qbootstrap-nav ul li > a:hover{
            color: white
        }
        .qbootstrap-nav ul li.active,.qbootstrap-nav ul li{
            background-color: grey
        }
        #page{
            min-height: 100vh
        }
        #content{
            max-width: 716px;
            margin: 0 auto;
            padding: 15px;
        }
        form{
            margin-top: 20px;
        }
    </style>
    </head>
    <body>

        <nav class="navbar navbar-default" style="background-color: #8282a2">
              <a href="#">
                    <?php
                    $data['menu'] = 'Company';
                    $this->load->view('admin/base-menu.php',$data);
                    ?>
              </a>
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <!-- <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="index.html">Company</a></li>
                <li><a href="index.html">Beranda</a></li>
                <li><a href="http://localhost/nuansanm/index.php/main/products">Produk</a></li>
                <li><a href="#testimony">Testimoni</a></li>
                <li><a href="http://localhost/nuansanm/index.php/main/about">Tentang Kami</a></li>
                <li><a href="#testimony">Kontak Kami</a></li>
              </ul> -->
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
    <div id="page">
        <style type="text/css">
            #content{
                padding: 0px 15px;
            }
        </style>

        <div id="content">
            <h3 class="text-center">Informasi Perusahaan</h3>
            <span>*Informasi ini akan ditampilkan di setiap halaman web</span>
            <form method="post" action="<?php echo site_url('admin/main/company');?>">
                <div class="form-group">
                    <label for="name" class="control-label">Nama Perusahaan</label>
                    <input type="hidden" class="form-control" name="user_id" value="<?php echo isset($user->user_id) ? $user->user_id : set_value('user_id'); ?>">
                    <input type="text" class="form-control" name="name" value="<?php echo isset($user->name) ? $user->name : set_value('name'); ?>" placeholder="Nama Perusahaan..." >
                </div>
                <div class="form-group">
                    <label for="emailx" class="control-label">Alamat</label>
                    <input type="text" class="form-control" value="<?php echo isset($user->email) ? $user->email : set_value('email'); ?>" name="emailx" placeholder="Alamat..." >
                </div>
                <div class="form-group">
                    <label for="emailx" class="control-label">Email</label>
                    <input type="text" class="form-control" value="<?php echo isset($user->email) ? $user->email : set_value('email'); ?>" name="emailx" placeholder="Email..." >
                </div>
                <div class="form-group">
                    <label for="emailx" class="control-label">No Telepon</label>
                    <input type="text" class="form-control" value="<?php echo isset($user->email) ? $user->email : set_value('email'); ?>" name="emailx" placeholder="No Telepon..." >
                </div>
                <div class="form-group">
                     <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                    <button class="btn btn-primary pull-right">Update</button>
                </div>
            </form>
        </div>
    </div>
    </body>
</html>
