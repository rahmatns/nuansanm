<?php
$this->load->view('admin/contact/header');
?>
<div id="bread-crumb">
    <a href="<?=base_url('admin/contact')?>">Contact</a> /
    <a class="active"> <?=$contact['name']?></a>
</div>
<div class="action-container">
    <div class="action-center">
        <div class="dropdown">
          <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Action
          <span class="caret"></span></button>
          <ul class="dropdown-menu action-menu">
            <li><a href="#deleteModal" data-toggle="modal">Delete</a></li>
          </ul>
        </div>
    </div>
</div>
<div id="content-detail">
    <form method="post" action="<?php echo site_url('admin/contact/edit/'.$contact['id']);?>">
        <div class="form-group">
            <label for="name" class="control-label">Nama</label>
            <input type="text" class="form-control" name="name" value="<?php echo isset($contact['name']) ? $contact['name'] : set_value('name'); ?>"
             placeholder="Nama..." required="required" />
        </div>
        <div class="form-group">
            <label for="email" class="control-label">Email</label>
            <input type="email" class="form-control" name="email" value="<?php echo isset($contact['email']) ? $contact['email'] : set_value('email'); ?>"
             placeholder="Email..." required="required" />
        </div>
        <div class="form-group">
            <label for="message" class="control-label">Pesan</label>
            <input type="text" class="form-control" name="message" value="<?php echo isset($contact['message']) ? $contact['message'] : set_value('message'); ?>"
             placeholder="Pesan..."/>
        </div>
        <div class="form-group pull-right">
            <a href="<?=base_url('admin/contact/view/'.$contact['id'])?>" class="btn btn-warning">Cancel</a>
            <button class="btn btn-primary">Update</button>
        </div>
    </form>
</div>
<?php
$this->load->view('admin/contact/footer');
?>
