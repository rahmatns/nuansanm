<?php
$this->load->view('admin/contact/header');
?>
<div id="bread-crumb">
    <a href="<?=base_url('admin/contact')?>">Contact</a> /
    <a class="active"> <?=$contact['name']?></a>
</div>
<div class="action-container">
    <div class="action-center">
        <div class="dropdown">
          <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Action
          <span class="caret"></span></button>
          <ul class="dropdown-menu action-menu">
            <li><a href="#deleteModal" data-toggle="modal">Delete</a></li>
          </ul>
        </div>
    </div>
</div>
<div id="content-detail">
        <div class="form-group">
            <label for="name" class="control-label">Nama</label>
            <input type="text" class="form-control" name="name" value="<?=$contact['name']?>"
             readonly="readonly" />
        </div>
        <div class="form-group">
            <label for="email" class="control-label">Email</label>
            <input type="email" class="form-control" name="email" value="<?=$contact['email']?>" readonly="readonly"/>
        </div>
        <div class="form-group">
            <label for="message" class="control-label">Pesan</label>
            <input type="text" class="form-control" name="message" value="<?=$contact['message']?>" readonly="readonly"/>
        </div>
        <div class="form-group pull-right">
            <a href="<?=base_url('admin/contact/edit/'.$contact['id'])?>" class="btn btn-warning">Edit</a>
        </div>
</div>
<?php
$this->load->view('admin/contact/footer');
?>
