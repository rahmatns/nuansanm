<?php
$this->load->view('admin/contact/header');
?>
<div id="bread-crumb">
    <a href="<?=base_url('admin/product')?>">Product</a> /
    <a class="active"> <?=$product['judul']?></a>
</div>
<div id="content">
    <form method="post" action="<?php echo site_url('admin/company/create');?>">
        <div class="form-group">
            <label for="judul" class="control-label">Judul</label>
            <input type="text" class="form-control" name="judul" value="<?php echo isset($product['judul']) ? $product['judul'] : set_value('judul'); ?>"
             placeholder="Judul..." required="required" />
        </div>
        <div class="form-group">
            <label for="slug" class="control-label">Slug</label>
            <input type="text" class="form-control" name="slug" value="<?php echo isset($product['slug']) ? $product['slug'] : set_value('slug'); ?>"/>
        </div>
        <div class="form-group">
            <label for="gambar" class="control-label">Gambar</label>
            <input type="file" class="form-control" name="gambar">
        </div>
        <div class="form-group">
            <label for="deskripsi" class="control-label">Deskripsi</label>
            <input type="text" class="form-control" name="deskripsi" value="<?php echo isset(
                $product['deskripsi']) ? $product['deskripsi'] : set_value('deskripsi'); ?>"/>
        </div>
        <div class="form-group pull-right">
            <a href="<?=base_url('admin/product/view/'.$product['id'])?>" class="btn btn-warning">Cancel</a>
            <button class="btn btn-primary">Create</button>
        </div>
    </form>
</div>
<?php
$this->load->view('admin/contact/footer');
?>
