<?php
$this->load->view('admin/contact/header');
?>
<div id="bread-crumb">
    <a href="<?=base_url('admin/contact')?>">Contact</a> /
    <a class="active"> New</a>
</div>
<div id="content-detail">
    <form method="post" action="<?php echo site_url('admin/contact/create');?>">
        <div class="form-group">
            <label for="name" class="control-label">Nama</label>
            <input type="text" class="form-control" name="name" value="<?=set_value('name')?>"
             placeholder="Nama..." required="required" >
        </div>
        <div class="form-group">
            <label for="email" class="control-label">Email</label>
            <input type="email" class="form-control" value="<?=set_value('email')?>" name="email" placeholder="Email..."  required="required">
        </div>
        <div class="form-group">
            <label for="message" class="control-label">Pesan</label>
            <input type="text" class="form-control" value="<?=set_value('message')?>" name="message" placeholder="Pesan..."  required="required">
        </div>
        <div class="form-group pull-right">
            <a href="<?=base_url('admin/contact')?>" class="btn btn-warning">Cancel</a>
            <button class="btn btn-primary">Create</button>
        </div>
    </form>
</div>
<?php
$this->load->view('admin/contact/footer');
?>
