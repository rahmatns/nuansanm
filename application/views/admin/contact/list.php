<?php
$this->load->view('admin/contact/header');
?>
<div id="bread-crumb">
    <a class="active">Contact</a>
    <div>
        <a href="<?=base_url('admin/contact/create')?>" class="btn btn-default">Create</a>
    </div>
</div>
<div id="content">
    <?php if (!isset($contacts) || count($contacts) == 0): ?>
        Belum ada kontak.
    <?php else: ?>
    <div class="table-responsive">
      <table class="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Nama</th>
            <th>Email</th>
            <th>Pesan</th>
            <th/>
          </tr>
        </thead>
        <tbody>
        <?php
        $counter = 1;
        foreach ($contacts as $contact): ?>
          <tr>
            <td><?=$counter++?></td>
            <td><?=$contact['name']?></td>
            <td><?=$contact['email']?></td>
            <td><?=$contact['message']?></td>
            <td><a href="<?=base_url('admin/contact/view/'.$contact['id'])?>" class="btn btn-sm btn-info">Lihat</a></td>
          </tr>
        <?php endforeach ?>
        </tbody>
      </table>
    </div>
    <?php endif ?>
</div>
<?php
$this->load->view('admin/contact/footer');
?>