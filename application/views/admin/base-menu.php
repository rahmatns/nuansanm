    <style type="text/css">
        #menu-container{
            padding: 20px;
            min-height: 100vh;
            min-width: 100vw;
            background: linear-gradient(to bottom right, #fdf431, #3e7b00);
        }
        #menu-icon{
            position: absolute;
            z-index: 11;
        }
        #menu-icon img{
            padding:8px;
            width: 50px;
            height: 50px;
        }
        .hidden{
            display: none;
        }
        .no-border{
            border: none;
            border-radius: 0px;
        }
        #menu-icon span{
            color: white
        }
    </style>
<div id="menu-icon">
    <img id="menu-open" onclick="show_menu()" class="btn no-border" src="<?=base_url('icons/menu-open-white.png')?>">
    <img id="menu-close" onclick="hide_menu()" class="btn no-border hidden" src="<?=base_url('icons/menu-close-white.png')?>">
    <span id="menu-span"><?= isset($menu) ? $menu : "NuansaNM"?></span>
</div>
<div id="menu-container" class="hidden">
    <nav class="navbar">
        <div class="container-fluid">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <?php
                        if(!empty($this->session->userdata('user_name'))){
                            echo "Halo, ".$this->session->userdata('user_name')." ";
                        }
                        ?>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="<?=base_url('admin/auth/logout')?>">Keluar</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <div class="row menu-list">
        <div class="menu-item">
            <a href="<?=base_url('admin/company')?>">
                <img src="<?=base_url('icons/menuitem_company.png')?>" class="img-responsive">
                <span class="caption">Company</span>
            </a>
        </div>
        <div class="menu-item">
            <a href="<?=base_url('admin/homepage')?>">
                <img src="<?=base_url('icons/menuitem_noicon.png')?>" class="img-responsive">
                <span class="caption">Homepage</span>
            </a>
        </div>
        <div class="menu-item">
            <a href="<?=base_url('admin/about')?>">
                <img src="<?=base_url('icons/menuitem_noiconi.png')?>" class="img-responsive">
                <span class="caption">About Us</span>
            </a>
        </div>
        <div class="menu-item">
            <a href="<?=base_url('admin/product')?>">
                <img src="<?=base_url('icons/menuitem_noiconii.png')?>" class="img-responsive">
                <span class="caption">Product</span>
            </a>
        </div>
        <div class="menu-item">
            <a href="<?=base_url('admin/gallery')?>">
                <img src="<?=base_url('icons/menuitem_gallery.png')?>" class="img-responsive">
                <span class="caption">Gallery</span>
            </a>
        </div>
        <div class="menu-item">
            <a href="<?=base_url('admin/testimony')?>">
                <img src="<?=base_url('icons/menuitem_noiconiii.png')?>" class="img-responsive">
                <span class="caption">Testimony</span>
            </a>
        </div>
        <div class="menu-item">
            <a href="<?=base_url('admin/contact')?>">
                <img src="<?=base_url('icons/menuitem_noiconiv.png')?>" class="img-responsive">
                <span class="caption">Contact</span>
            </a>
        </div>
    </div>
</div>
<script type="text/javascript">
    function show_menu(argument) {
        console.log("show_menu");
        document.querySelector("#menu-span").classList.add("hidden");
        document.querySelector("#menu-open").classList.add("hidden");
        document.querySelector("#menu-container").classList.remove("hidden");
        document.querySelector("#menu-close").classList.remove("hidden");
    }
    function hide_menu(argument) {
        console.log("hide_menu");
        document.querySelector("#menu-span").classList.remove("hidden");
        document.querySelector("#menu-open").classList.remove("hidden");
        document.querySelector("#menu-container").classList.add("hidden");
        document.querySelector("#menu-close").classList.add("hidden");
    }
</script>