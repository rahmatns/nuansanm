<!DOCTYPE html>
<html>
<head>
    <title>PT. Nuansa Nisa Met &mdash; Jasa Bodem dan Roll Plat: Menerima pengerjaan Pembuatan tangki, Bodem, dan Roll Plat</title>
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.css">
    <style type="text/css">
        body{
            padding: 16px;
            background-color: lime;
            min-height: 100vh;
            min-width: 100vw;
            background: linear-gradient(to bottom right, #fdf431, #3e7b00);
            font-size: 14pt;
        }

        .panel-container{
            max-width: 760px;
            /*margin: 0 auto;*/

            margin: 0;
            position: absolute;
            top: 50%;
            left: 50%;
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
        }
        .panel-heading p{
            font-size: 20pt;
            text-align: right;
        }
        .panel-heading span{
            text-align: right;
        }
        .panel-heading .row{
            margin: 0px;
            padding: 10px;
            display: inline-flex;
            /*background-color: red;*/
        }
        .panel-heading{
            /*background-color: lime !important;*/
            padding: 0px;
        }
        .panel-body{
            padding:50px;
        }
    </style>
</head>
<body>
    <div class="panel panel-default panel-container">
        <div class="panel-heading">
            <div class="row">
                <div>
                    <p>Admin Panel</p>
                    <span>Silakan isi email dan password</span>
                </div>
                <div style="width: 1px;background-color: grey;margin: 0 15px">
                </div>
                <div>
                    <img class="img img-responsive" src="<?=base_url('icons/lock.png')?>" width=70px>
                </div>
            </div>
        </div>
            <div class="panel-body">
                <form action="<?php echo base_url('admin/auth/login'); ?>" method="post">
                    <div class="row">
                        <div class="form-group">
                          <label for="email" class="control-label">Email</label>
                          <input type="email" class="form-control" id="email" name="email" required="1" title="Please enter you email" placeholder="example@gmail.com">
                          <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                          <label for="password" class="control-label">Password</label>
                          <input type="password" class="form-control" id="password" name="password" required="1" title="Please enter your password" placeholder="Password..">
                          <span class="help-block"></span>
                        </div>
                        <button type="submit" class="btn btn-success btn-block">Login</button>
                    </div>
                </form>
                <?php if ($this->session->flashdata('error')): ?>
                    <div class="alert alert-danger">
                      <span><?=$this->session->flashdata('error')?></span>
                    </div>
                <?php endif ?>
            </div>
    </div>
</body>
</html>