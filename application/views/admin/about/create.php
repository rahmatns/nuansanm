<?php
$this->load->view('admin/about/header');
?>
<div id="bread-crumb">
    <a class="active">About</a>
</div>
<div id="content-detail">
    <?php echo form_open_multipart(site_url('admin/about/create'));?>
        <div class="form-group">
            <label for="email" class="control-label">Gambar</label>
            <input type="file" class="form-control" name="image">
        </div>
        <div class="form-group">
            <label for="name" class="control-label">Judul</label>
            <input type="text" class="form-control" name="name" value="<?php echo isset($about['name']) ? $about['name'] : set_value('name'); ?>" placeholder="Judul artikel">
        </div>
        <div class="form-group">
            <label for="description" class="control-label">Deskripsi</label>
            <textarea id="summernote" name="description"  placeholder="Artikel..">
                <?php echo isset($about['description']) ? $about['description'] : set_value('description'); ?>
            </textarea>
        </div>
        <div class="form-group pull-right">
            <button type="submit" class="btn btn-primary">Create</button>
        </div>
    </form>
</div>
<?php
$this->load->view('admin/about/footer');
?>