<?php
$this->load->view('admin/about/header');
?>
<div id="bread-crumb">
    <a class="active">About</a>
</div>
<div class="content-detail">
    <?php if (isset($error)): ?>
        <?php echo $error ?>
    <?php endif ?>
    <div style="max-width: 760px;margin: 0 auto;background-color: #f1f1f1;min-height: 100vh">
        <?php echo form_open_multipart(site_url('admin/about/edit'));?>
            <div class="form-group pull-right" style="padding: 15px">
                <a href="<?=base_url('admin/about')?>" class="btn btn-warning">Cancel</a>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
            <div class="row" style="padding: 15px">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="image" class="control-label">Gambar</label><br/>
                        <img src="<?=site_url($about['image_url'])?>" class="img img-responsive">
                        <input type="file" class="form-control" name="image">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name" class="control-label">Judul</label>
                        <input type="text" class="form-control" name="name" value="<?php echo isset($about['name']) ? $about['name'] : set_value('name'); ?>" placeholder="Judul artikel">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="address" class="control-label">Deskripsi</label>
                        <textarea id="summernote" name="description"  placeholder="Artikel..">
                            <?php echo isset($about['description']) ? $about['description'] : set_value('description'); ?>
                        </textarea>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
$this->load->view('admin/about/footer');
?>