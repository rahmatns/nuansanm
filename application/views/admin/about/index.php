<?php
$this->load->view('admin/about/header');
?>
<div id="bread-crumb">
    <a class="active">About</a>
</div>
<div class="content-detail">
    <div style="max-width: 760px;margin: 0 auto;background-color: #f1f1f1;min-height: 100vh">
        <div class="pull-right" style="padding: 15px">
            <a href="<?=base_url('admin/about/edit')?>" class="btn btn-warning">Edit</a>
        </div>
        <div class="row" style="padding: 15px">
            <div class="col-md-12">
                <img src="<?=site_url($about['image_url'])?>" class="img img-responsive">
            </div>
            <div class="col-md-12">
                <h1><?=$about['name']?></h1>
            </div>
            <div class="col-md-12">
                <div style="padding: 15px">
                    <?=$about['description']?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('admin/about/footer');
?>