<?php
$this->load->view('admin/product/header');
?>
<div id="bread-crumb">
    <a class="active">Product</a>
    <div>
        <a href="<?=base_url('admin/product/create')?>" class="btn btn-default">Create</a>
    </div>
</div>
<div id="content">
    <?php if (!isset($products) || count($products) == 0): ?>
        Belum ada product.
    <?php else: ?>
    <div class="table-responsive">
      <table class="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Judul</th>
            <th>Slug</th>
            <th>Deskripsi</th>
            <th>Gambar</th>
            <th/>
          </tr>
        </thead>
        <tbody>
        <?php
        $counter = 1;
        foreach ($products as $product): ?>
          <tr>
            <td><?=$counter++?></td>
            <td><?=$product['name']?></td>
            <td><?=$product['slug']?></td>
            <td><?=$product['description']?></td>
            <td>
                <img src="<?=base_url($product['image_url'])?>" width="100px" height="100px">
            </td>
            <td><?=$product['enable']?></td>
            <td><a href="<?=base_url('admin/product/view/'.$product['id'])?>" class="btn btn-sm btn-info">Lihat</a></td>
          </tr>
        <?php endforeach ?>
        </tbody>
      </table>
    </div>
    <?php endif ?>
</div>
<?php
$this->load->view('admin/product/footer');
?>