<?php
$this->load->view('admin/product/header');
?>
<div id="bread-crumb">
    <a href="<?=base_url('admin/product')?>">Product</a> /
    <a class="active"> <?=$product['name']?></a>
</div>
<div class="action-container">
    <div class="action-left">
        <?php if ($product['enable'] == TRUE): ?>
            <form method="post" action="<?=base_url('admin/product/disable/'.$product['id'])?>">
                <input type="hidden" name="enable">
                <button class="btn btn-default" type="submit" title="Disable from homepage ?">REMOVE FROM HOMEPAGE</button>
            </form>
        <?php else: ?>
            <form method="post" action="<?=base_url('admin/product/enable/'.$product['id'])?>">
                <input type="hidden" name="enable">
                <button class="btn btn-default" type="submit" title="Show in homepage ?">SHOW IN HOMEPAGE</button>
            </form>
        <?php endif ?>
    </div>
    <div class="action-center">
        <div class="dropdown" title="Click to show action">
          <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Action
          <span class="caret"></span></button>
          <ul class="dropdown-menu action-menu">
            <li><a href="#deleteModal" data-toggle="modal">Delete</a></li>
          </ul>
        </div>
    </div>
    <div class="action-right"></div>
</div>
<div id="content-detail">
    <?php if (isset($error)): ?>
        <div class="row">
            <div class="alert alert-danger">
                <span class="text-center"><?=$error?></span>
            </div>
        </div>
    <?php endif ?>
    <?php echo form_open_multipart();?>
        <div class="form-group">
            <label for="name" class="control-label">Judul</label>
            <input type="text" class="form-control" name="name" value="<?php echo isset($product['name']) ? $product['name'] : set_value('name'); ?>"
             placeholder="Judul..." required="required" />
        </div>
        <div class="form-group">
            <label for="slug" class="control-label">Slug</label>
            <input type="text" class="form-control" name="slug" value="<?php echo isset($product['slug']) ? $product['slug'] : set_value('slug'); ?>"/>
        </div>
        <div class="form-group">
            <label for="image" class="control-label">Gambar</label><br/>
            <img src="<?=base_url($product['image_url'])?>" alt="<?=$product['name']?>" width="100px" height="100px"/><br/>
            <input type="file" class="form-control" name="image">
        </div>
        <div class="form-group">
            <label for="description" class="control-label">Deskripsi</label>
            <input type="text" class="form-control" name="description" value="<?php echo isset(
                $product['description']) ? $product['description'] : set_value('description'); ?>"/>
        </div>
        <div class="form-group pull-right">
            <a href="<?=base_url('admin/product/view/'.$product['id'])?>" class="btn btn-warning">Cancel</a>
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
    </form>
</div>
<?php
$this->load->view('admin/product/footer');
?>
