<?php
$this->load->view('admin/product/header');
?>
<div id="bread-crumb">
    <a href="<?=base_url('admin/product')?>">Product</a> /
    <a class="active"> New</a>
</div>
<div id="content-detail">
    <?php if (isset($error)): ?>
        <div class="row">
            <div class="alert alert-danger">
                <span class="text-center"><?=$error?></span>
            </div>
        </div>
    <?php endif ?>
    <?php echo form_open_multipart(site_url('admin/product/create'));?>
        <div class="form-group">
            <label for="title" class="control-label">Judul</label>
            <input type="text" class="form-control" name="name" value="<?=set_value('name')?>"
             placeholder="Judul..." required="required" >
        </div>
        <div class="form-group">
            <label for="slug" class="control-label">Slug</label>
            <input type="text" class="form-control" value="<?=set_value('slug')?>" name="slug" placeholder="Slug..." >
        </div>
        <div class="form-group">
            <label for="image" class="control-label">Gambar</label>
            <input type="file" class="form-control" name="image">
        </div>
        <div class="form-group">
            <label for="description" class="control-label">Deskripsi</label>
            <textarea id="summernote" name="description"  placeholder="Deskripsi..." required="required">
                <?=set_value('description')?>
            </textarea>
        </div>
        <div class="form-group pull-right">
            <a href="<?=base_url('admin/product')?>" class="btn btn-warning">Cancel</a>
            <button type="submit" class="btn btn-primary">Create</button>
        </div>
    </form>
</div>
<?php
$this->load->view('admin/product/footer');
?>
