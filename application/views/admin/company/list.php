<?php
$this->load->view('admin/company/header');
?>
<div id="bread-crumb">
    <a class="active">Company</a>
    <div>
    <?php if (!isset($companies) || count($companies) == 0): ?>
        <a href="<?=base_url('admin/company/create')?>" class="btn btn-default">Create</a>
    <?php endif ?>
    </div>
</div>
<div id="content">
    <?php if (!isset($companies) ||count($companies) == 0): ?>
        Belum ada ada.
    <?php else: ?>
    <div class="table-responsive">
      <table class="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th>Email</th>
            <th>No Telepon</th>
            <th/>
          </tr>
        </thead>
        <tbody>
        <?php
        $counter = 1;
        foreach ($companies as $company): ?>
          <tr>
            <td><?=$counter++?></td>
            <td><?=$company['name']?></td>
            <td><?=$company['address']?></td>
            <td><?=$company['email']?></td>
            <td><?=$company['phone']?></td>
            <td><a href="<?=base_url('admin/company/view/'.$company['id'])?>" class="btn btn-sm btn-info">Lihat</a></td>
          </tr>
        <?php endforeach ?>
        </tbody>
      </table>
    </div>
    <?php endif ?>
</div>
<?php
$this->load->view('admin/company/footer');
?>