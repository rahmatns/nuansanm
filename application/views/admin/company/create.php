<?php
$this->load->view('admin/company/header');
?>
<div id="bread-crumb">
    <a href="<?=base_url('admin/company')?>">Company</a> /
    <a class="active"> New</a>
</div>
<div id="content">
    <form method="post" action="<?php echo site_url('admin/company/create');?>">
        <div class="form-group">
            <label for="name" class="control-label">Nama Perusahaan</label>
            <input type="text" class="form-control" name="name" value="<?=set_value('name')?>"
             placeholder="Nama Perusahaan..." required="required" >
        </div>
        <div class="form-group">
            <label for="address" class="control-label">Alamat</label>
            <input type="text" class="form-control" value="<?=set_value('address')?>" name="address" placeholder="Alamat..." >
        </div>
        <div class="form-group">
            <label for="email" class="control-label">Email</label>
            <input type="email" class="form-control" value="<?=set_value('email')?>" name="email" placeholder="Email..." >
        </div>
        <div class="form-group">
            <label for="phone" class="control-label">No Telepon</label>
            <input type="text" class="form-control" value="<?=set_value('phone')?>" name="phone" placeholder="No Telepon..." >
        </div>
        <div class="form-group pull-right">
            <a href="<?=base_url('admin/company')?>" class="btn btn-warning">Cancel</a>
            <button class="btn btn-primary">Create</button>
        </div>
    </form>
</div>
<?php
$this->load->view('admin/company/footer');
?>
