<?php
$this->load->view('admin/company/header');
?>
<div id="bread-crumb">
    <a href="<?=base_url('admin/company')?>">Company</a> /
    <a class="active"> <?=$company['name']?></a>
</div>
<div id="content">
        <div class="form-group">
            <label for="name" class="control-label">Nama Perusahaan</label>
            <input type="text" class="form-control" name="name" value="<?=$company['name']?>" placeholder="Nama Perusahaan..." readonly="readonly">
        </div>
        <div class="form-group">
            <label for="address" class="control-label">Alamat</label>
            <input type="text" class="form-control" value="<?=$company['address']?>" name="address" placeholder="Alamat..." readonly="readonly">
        </div>
        <div class="form-group">
            <label for="email" class="control-label">Email</label>
            <input type="email" class="form-control" value="<?=$company['email']?>" placeholder="Email..." readonly="readonly">
        </div>
        <div class="form-group">
            <label for="phone" class="control-label">No Telepon</label>
            <input type="text" class="form-control" value="<?=$company['phone']?>" name="phone" placeholder="No Telepon..." readonly="readonly">
        </div>
        <div class="form-group pull-right">
            <a href="<?=base_url('admin/company/edit/'.$company['id'])?>" class="btn btn-warning">Edit</a>
        </div>
</div>
<?php
$this->load->view('admin/company/footer');
?>