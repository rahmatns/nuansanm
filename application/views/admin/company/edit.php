<?php
$this->load->view('admin/company/header');
?>
<div id="bread-crumb">
    <a href="<?=base_url('admin/company')?>">Company</a> /
    <a class="active"> <?=$company['name']?></a>
</div>
<div id="content">
    <form method="post" action="<?php echo site_url('admin/company/edit/'.$company['id']);?>">
        <div class="form-group">
            <label for="name" class="control-label">Nama Perusahaan</label>
            <input type="hidden" class="form-control" name="user_id" value="<?php echo isset($user->user_id) ? $user->user_id : set_value('user_id'); ?>">
            <input type="text" class="form-control" name="name" value="<?=$company['name']?>" placeholder="Nama Perusahaan..." >
        </div>
        <div class="form-group">
            <label for="address" class="control-label">Alamat</label>
            <input type="text" class="form-control" value="<?=$company['address']?>" name="address" placeholder="Alamat..." >
        </div>
        <div class="form-group">
            <label for="email" class="control-label">Email</label>
            <input type="email" class="form-control" value="<?=$company['email']?>" name="email" placeholder="Email..." >
        </div>
        <div class="form-group">
            <label for="phone" class="control-label">No Telepon</label>
            <input type="text" class="form-control" value="<?=$company['phone']?>" name="phone" placeholder="No Telepon..." >
        </div>
        <div class="form-group pull-right">
            <a href="<?=base_url('admin/company/view/'.$company['id'])?>" class="btn btn-warning">Cancel</a>
            <button class="btn btn-primary">Update</button>
        </div>
    </form>
</div>
<?php
$this->load->view('admin/company/footer');
?>