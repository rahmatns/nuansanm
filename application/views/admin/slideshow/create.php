<?php
$data['active'] = 'slideshow';
$this->load->view('admin/homepage/header',$data);
?>
<div id="bread-crumb">
    <a href="<?=base_url('admin/homepage')?>">Homepage</a> /
    <a href="<?=base_url('admin/homepage/slideshow')?>"> Slideshow</a> /
    <a class="active"> New</a>
</div>
<div id="content-detail">
    <?php if (isset($error)): ?>
        <div class="row">
            <div class="alert alert-danger">
                <span class="text-center"><?=$error?></span>
            </div>
        </div>
    <?php endif ?>
    <?php echo form_open_multipart(site_url('admin/slideshow/create'));?>
        <div class="form-group">
            <label for="name" class="control-label">Judul</label>
            <input type="text" class="form-control" name="name" value="<?=set_value('name')?>"
             placeholder="Judul..." required="required" >
        </div>
        <div class="form-group">
            <label for="description" class="control-label">Subjudul</label>
            <input type="text" class="form-control" value="<?=set_value('description')?>" name="description" placeholder="Subjudul..." >
        </div>
        <div class="form-group">
            <label for="image" class="control-label">Gambar</label>
            <input type="file" class="form-control" name="image">
        </div>
        <div class="form-group">
            <label for="sequence" class="control-label">Sequence</label>
            <input type="number" class="form-control" value="<?=set_value('sequence')?>" name="sequence">
        </div>
        <div class="form-group pull-right">
            <a href="<?=base_url('admin/homepage/slideshow')?>" class="btn btn-warning">Cancel</a>
            <button class="btn btn-primary">Create</button>
        </div>
    </form>
</div>
<?php
$this->load->view('admin/slideshow/footer');
?>
