<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>PT. Nuansa Nisa Met &mdash; Jasa Bodem dan Roll Plat: Menerima pengerjaan Pembuatan tangki, Bodem, dan Roll Plat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="PT. Nuansa Nisa Met &mdash; Jasa Bodem dan Roll Plat: Menerima pengerjaan Pembuatan tangki, Bodem, dan Roll Plat" />
    <meta name="keywords" content="Nuansa Nisa Met, Tangki, Bodem, Roll Plat" />
    <meta name="author" content="PT. Nuansa Nisa Met" />

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet">

    <!-- Animate.css -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/magnific-popup.css">
    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/owl.theme.default.min.css">
    <!-- Flexslider  -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/flexslider.css">
    <!-- Theme style  -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/style.css">

    <!-- Modernizr JS -->
    <script src="<?=base_url()?>assets/js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="<?=base_url()?>assets/js/respond.min.js"></script>
    <![endif]-->
    </head>
    <?php
    $about_img_url = base_url('assets/images/blog-1.jpg');
    ?>
    <body>
    <div id="page">
    <?php
    $data['active'] = 'about';
    $this->load->view('frontend-menu',$data);
    ?>
    <div>
        <div class="jumbotron text-xs-center" style="background-color: white;padding: 15px;text-align: center;">
  <h1 class="display-3">Maaf !</h1>
  <p class="lead">Halaman yang anda cari tidak tersedia.</p>
  <hr>
  <p>
    Silakan hubungi administrator melalui formulir di bawah
  </p>
  <p class="lead">
    <a class="btn btn-primary btn-sm" href="#contact" role="button">Kontak Kami</a>
  </p>
</div>
    </div>

