<?php
if(!isset($active)){
    $active = "none";
}
if(!isset($product_list)){
    $product_list = False;
}
?>
<nav class="qbootstrap-nav" role="navigation">
    <div class="top-menu">
        <div class="row">
            <div class="col-xs-12">
                <div style="height: 3px;background-color: #8282a2"></div>
            </div>
            <div class="col-xs-12 text-center">
                <div class="menu-1">
                    <ul>
                        <li class="<?=$active == 'home' ? 'active' : ''?>">
                            <a href="<?=$active == 'home' ? '#' : base_url()?>">Beranda</a>
                        </li>
                        <li class="<?=$active == 'product' ? 'active' : ''?>">
                            <a href="<?=$product_list == 'True' ? '#' : base_url('main/products')?>">Produk</a>
                        </li>
                        <li class="<?=$active == 'gallery' ? 'active' : ''?>">
                            <a href="<?=$active == 'gallery' ? '#' : base_url('main/gallery')?>">Galeri</a>
                        </li>
                        <li>
                            <a href="<?=$active == 'home' ? '#testimony' : base_url('#testimony')?>">Testimoni</a></li>
                        <li class="<?=$active == 'about' ? 'active' : ''?>">
                            <a href="<?=$active == 'about' ? '#' : base_url('main/about')?>">Tentang Kami</a>
                        </li>
                        <li class="btn-cta">
                            <a href="#contact"><span>Kontak Kami <i class="icon-mail5"></i></span></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="top">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12 text-center">
                            <div id="qbootstrap-logo">
                                <a href="<?=base_url()?>">
                                    <img src="<?=base_url($company['image_url'])?>" width="100px">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-8 col-xs-6">
                            <h1 class="title">
                            <?=$company['display_name']?>
                            </h1>
                            <span style="margin-bottom: 15px"><?=$company['description']?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</nav>