
    <footer id="contact" style="background-color: rgba(0,0,0,0.9);padding-top: 50px">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel panel-heading"><h4 class="text-center"><?=$company['name']?></h4></div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <td style="border-top: none;">Alamat</td>
                                        <td style="border-top: none;">:</td>
                                        <td style="border-top: none;"><?=$company['address']?></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>:</td>
                                        <td><?=$company['email']?></td>
                                    </tr>
                                    <tr>
                                        <td>Telepon</td>
                                        <td>:</td>
                                        <td><?=$company['phone']?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h4 class="text-center">Hubungi Kami</h4></div>
                        <div class="panel-body">
                            <div id="info-container">
                                <!-- <div class="alert alert-success alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  <strong>Sukses!</strong> Pesan anda sudah kami terima.
                                </div> -->
                            </div>
                            <form action="<?=base_url('main/post_contact')?>" method="post" id="form-contact-us">
                                <div class="form-group">
                                    <input type="name" class="form-control" id="name" autocomplete="name" placeholder="Nama" required="required">
                                </div>
                                <div class="form-group">
                                    <label for="email" class="sr-only">Email</label>
                                    <input type="email" class="form-control" id="email" autocomplete="email" placeholder="Email" required="required">
                                </div>
                                <div class="form-group">
                                    <label for="message" class="sr-only">Pesan</label>
                                    <textarea name="message" class="form-control" id="message" rows="3" placeholder="Tulis pesan anda di sini" required="required"></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" id="btn-submit" class="btn btn-success">Kirim</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row copyright">
            <div class="col-md-12 text-center">
                <p>
                    <small class="block">&copy; <?=date('Y')?> <?=$company['name']?></small>
                    <small class="block"><?=$company['email']?></small>
                </p>
            </div>
        </div>
    </footer>
    </div>

    <div class="gototop js-top">
        <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
    </div>
    <!-- jQuery -->
    <script src="<?=base_url()?>assets/js/jquery.min.js"></script>
    <!-- jQuery Easing -->
    <script src="<?=base_url()?>assets/js/jquery.easing.1.3.js"></script>
    <!-- Bootstrap -->
    <script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
    <!-- Waypoints -->
    <script src="<?=base_url()?>assets/js/jquery.waypoints.min.js"></script>
    <!-- Stellar Parallax -->
    <script src="<?=base_url()?>assets/js/jquery.stellar.min.js"></script>
    <!-- Carousel -->
    <script src="<?=base_url()?>assets/js/owl.carousel.min.js"></script>
    <!-- Flexslider -->
    <script src="<?=base_url()?>assets/js/jquery.flexslider-min.js"></script>
    <!-- countTo -->
    <script src="<?=base_url()?>assets/js/jquery.countTo.js"></script>
    <!-- Magnific Popup -->
    <script src="<?=base_url()?>assets/js/jquery.magnific-popup.min.js"></script>
    <script src="<?=base_url()?>assets/js/magnific-popup-options.js"></script>
    <!-- Sticky Kit -->
    <script src="<?=base_url()?>assets/js/sticky-kit.min.js"></script>
    <!-- Main -->
    <script src="<?=base_url()?>assets/js/main.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#form-contact-us').submit(function (event) {
                event.preventDefault();
                var name = $("input#name").val();
                var email = $("input#email").val();
                var message = $("textarea#message").val();
                jQuery.ajax({
                    type: "POST",
                    url: '<?=base_url('main/post_contact')?>',
                    dataType: 'json',
                    data: {
                        name:name,
                        email:email,
                        message:message
                    },
                    success:function (res) {
                        if(res.type == 'success'){
                            $("#info-container").html(
                                '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+res.message+'</div>');
                            $("input#name").val('');
                            $("input#email").val('');
                            $("textarea#message").val('');
                        }else{
                            $("#info-container").html(
                                '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+res.message+'</div>');
                        }
                    }
                });
            });
        });
    </script>
    </body>
</html>

