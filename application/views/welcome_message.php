<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title>PT. Nuansa Nisa Met &mdash; Jasa Bodem dan Roll Plat: Menerima pengerjaan Pembuatan tangki, Bodem, dan Roll Plat</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="PT. Nuansa Nisa Met &mdash; Jasa Bodem dan Roll Plat: Menerima pengerjaan Pembuatan tangki, Bodem, dan Roll Plat" />
	<meta name="keywords" content="Nuansa Nisa Met, Tangki, Bodem, Roll Plat" />
	<meta name="author" content="PT. Nuansa Nisa Met" />

	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet">

	<!-- Animate.css -->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.css">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/magnific-popup.css">
	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/owl.theme.default.min.css">
	<!-- Flexslider  -->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/flexslider.css">
	<!-- Theme style  -->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/style.css">

	<!-- Modernizr JS -->
	<script src="<?=base_url()?>assets/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="<?=base_url()?>assets/js/respond.min.js"></script>
	<![endif]-->
	</head>
	<body>
	<div class="qbootstrap-loader"></div>
	<div id="page">
	<?php
	$data['active'] = 'home';
	$this->load->view('frontend-menu',$data);
	?>

	<aside id="qbootstrap-hero">
		<div class="flexslider">
			<ul class="slides">
				<?php foreach ($slides as $slide): ?>
			   	<li style="background-image: url(<?=base_url($slide['image_url'])?>);">
			   		<div class="overlay"></div>
			   		<div class="container">
			   			<div class="row">
				   			<div class="col-md-8 col-md-offset-2 text-center slider-text">
				   				<div class="slider-text-inner">
				   					<h1><strong><?=$slide['name']?></strong></h1>
									<h2 class="doc-holder"><?=$slide['description']?></h2>
				   				</div>
				   			</div>
				   		</div>
			   		</div>
			   	</li>
				<?php endforeach ?>
		  	</ul>
	  	</div>
	</aside>
	<?php if (count($product_list) > 0): ?>
	<div id="product">
		<div class="row">
			<div class="container" style="padding: 15px">
				<a class="pull-left" style="color: rgba(0,0,0,.7);font-weight: 600;font-size: 16pt">PRODUK</a>
				<a class="pull-right" href="<?=base_url('main/products')?>">
					Lihat Semua
				</a>
			</div>
		</div>
		<?php
		$data['product_list'] = $product_list;
		$this->load->view('product_list',$data) ?>
	</div>
	<?php endif ?>

	<div id="testimony" class="qbootstrap-bg-section">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-6 col-md-offset-3 text-center qbootstrap-heading">
					<h2><span>Testimoni</h2>
					<p>Apa kata mereka tentang kami ?</p>
				</div>
			</div>
			<div class="row">
				<?php
				if (count($testimonies) == 1): ?>
					<?php foreach ($testimonies as $testimony): ?>
					<div class="col-sm-2"></div>
					<div class="col-sm-8 animate-box">
						<div class="testimony text-center">
							<span class="img-user" style="background-image: url(<?=base_url($testimony['image_url'])?>"></span>
							<span class="user"><?=$testimony['name']?></span>
							<blockquote>
								<p><?=$testimony['description']?></p>
							</blockquote>
						</div>
					</div>
					<?php endforeach ?>
				<?php elseif(count($testimonies) == 2): ?>
					<?php foreach ($testimonies as $testimony): ?>
						<div class="col-md-6 animate-box">
							<div class="testimony text-center">
								<span class="img-user" style="background-image: url(<?=base_url($testimony['image_url'])?>);"></span>
								<span class="user"><?=$testimony['name']?></span>
								<blockquote>
									<p><?=$testimony['description']?></p>
								</blockquote>
							</div>
						</div>
					<?php endforeach ?>
				<?php else: ?>
					<?php foreach ($testimonies as $testimony): ?>
					<div class="col-md-4 animate-box" style="margin-bottom: 70px">
						<div class="testimony text-center">
							<span class="img-user" style="background-image: url(<?=base_url($testimony['image_url'])?>);"></span>
							<span class="user"><?=$testimony['name']?></span>
							<blockquote>
								<p><?=$testimony['description']?></p>
							</blockquote>
						</div>
					</div>
					<?php endforeach ?>
				<?php endif ?>
			</div>
		</div>
	</div>