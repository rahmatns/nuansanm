<?php
class Auth_model extends CI_Model {

    protected $table_name = 'user';

    public function __construct(){
        $this->load->database();
    }

    public function search($where,$limit = FALSE){
        if (count($where) > 0){
            $this->db->where($where);
        }
        if ($limit !== FALSE){
            $this->db->limit($limit);
        }
        $query = $this->db->get($this->table_name);
        return $query->result_array();
    }

    public function browse($id){
        $query = $this->db->get_where($this->table_name, array('id' => $id));
        return $query->row();
    }
}