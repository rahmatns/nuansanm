<?php
class Company_model extends CI_Model {

    protected $table_name = 'company';

    public function __construct(){
        $this->load->database();
    }

    public function insert($data){
        $this->db->insert($this->table_name,$data);
        return $this->db->insert_id();
    }

    public function search($where,$limit = FALSE){
        if (count($where) > 0){
            $this->db->where($where);
        }
        if ($limit !== FALSE){
            $this->db->limit($limit);
        }
        $query = $this->db->get($this->table_name);
        return $query->result_array();
    }

    public function browse($id){
        $query = $this->db->get_where($this->table_name, array('id' => $id));
        return $query->row();
    }

    public function update($data,$id=FALSE){
        if ($id !== FALSE){
            $this->db->where('id',$id);
        }
        $this->db->update($this->table_name, $data);
    }
}